#! /usr/bin/env python
# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics
# Course code:	DD2404
# Date:		2012-12-08

import sys
import commands
import sqlite3
from prettytable import PrettyTable as PT

def main():

    #open a connection to the database
    conn = sqlite3.connect('./data/protdb')

    #create a cursor in the current database
    c = conn.cursor()

    print '1.Print the schema of the current database:'
    #1.print the schema of the loaded database
    c.execute("SELECT sql FROM sqlite_master WHERE type='table';")
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()

    print '2.Species in the database:'
    #2.print all the species in the database
    c.execute("SELECT * FROM species;")
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()

    print "3.Adding species 'Sus scrofa':"
    #3.add a new species to the database
    c.execute("INSERT OR IGNORE INTO species VALUES ('Ss', 'Sus scrofa', 'Scrofa');")
    c.execute("SELECT * FROM species;")
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()

    print "4.Proteins longer than 1000 aa:"
    #4.count the proteins with a sequence longer than 1000
    c.execute("SELECT accession FROM protein WHERE LENGTH(sequence)>1000;")
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()
    
    print "5.Species present in family NHR3:"
    #5.print species names matching NHR3 family
    c.execute('''
    SELECT DISTINCT(name)
    FROM species AS S, familymembers AS FM, protein as P
    WHERE P.species = S.abbrev
    AND FM.protein = P.accession
    AND FM.family = 'NHR3';
    '''
    )
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()
    
    print "6.Number of proteins from each species:"
    #6.count the proteins matching each family
    c.execute('''
    SELECT name, COUNT(accession)
    FROM species, protein
    WHERE protein.species = species.abbrev
    GROUP BY protein.species
    '''
    )
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()
    
    print "7.Adding structure information:"
    #7.create two more tables, for storing structures and their owners
    c.executescript('''
    CREATE TABLE IF NOT EXISTS structure (
        name character varying(100) PRIMARY KEY,
        resolution character varying(10),
        method character varying(50)
    );
    CREATE TABLE IF NOT EXISTS proteinmembers (
        protein character varying(20),
        structure character varying(100),
        PRIMARY KEY(protein,structure)
    );
    '''
    )
    c.execute("SELECT sql FROM sqlite_master WHERE type='table';")
    col_headers = [t[0] for t in c.description]
    printdb(c.fetchall(),col_headers)
    raw_input()

    #close the connection to the database
    conn.close()
    return 0

def printdb(pS,pC=('',)):
    '''
    Using the module prettytable, more stylish
    '''
    x = PT(pC)
    x.align = 'l'
    for r in pS:
        x.add_row(r)
    print x

main()
