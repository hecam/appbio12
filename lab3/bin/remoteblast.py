#! /usr/bin/env python2
# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics
# Course code:	DD2404
# Date:		2012-12-02

import sys
from Bio.Blast.NCBIWWW import qblast
from Bio import SeqIO

def main():

    if len(sys.argv)==1:
        sys.stderr.write("ERROR: missing filename\n")
        sys.stderr.write("Usage: ./remoteblast.py FILE\n")
        sys.exit()
    try:
        f = open(sys.argv[1])
        if f.read(1)!=">":
            sys.stderr.write("ERROR: {0} is probably not a FASTA file!\n".format(sys.argv[1]))
            sys.stderr.write("Input file must contain a protein sequence in FASTA format\n".format(sys.argv[1]))
            sys.exit()
        f.seek(0)
    except IOError as fErr:
        sys.stderr.write("ERROR: cannot open {0}!\n".format(sys.argv[1]))
        sys.exit()

    s=f.read()
    lXML=list(qblast("blastp", "nr", s))
    for ln in lXML:
        print ln,

    return 0

main()
