#! /usr/bin/env python2
# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics
# Course code:	DD2404
# Date:		2012-12-02

import sys
import tempfile
from Bio.Blast import NCBIXML

def main():

    if len(sys.argv)<3:
        sys.stderr.write("Usage: ./blastparse.py PATTERN FILE\n")
        sys.exit()
    if sys.argv[2]=="-":
        aux = sys.stdin
        f = tempfile.NamedTemporaryFile()
        f.write(aux.read())
        f.seek(0)
    else:
        f = open(sys.argv[2])
    try:
        if f.read(5)!="<?xml":
            sys.stderr.write("ERROR: {0} is probably not an XML file!\n".format(sys.argv[2]))
            sys.exit()
        f.seek(0)
    except IOError as fErr:
        sys.stderr.write("ERROR: cannot open {0}!\n".format(sys.argv[2]))
        sys.exit()

    raw=f.read()
    f.close()
    splitStr='<?xml'
    lXML=raw.split(splitStr)[1:]
    for root in lXML:
        root=splitStr+root
        f=tempfile.NamedTemporaryFile()
        f.write(root)
        f.seek(0)
        strQuery=getQueryString(root)
        par=NCBIXML.parse(f)
        for blast_rec in list(par):
            for alig in blast_rec.alignments:
                for hsp in alig.hsps:
                    if hsp.expect < 1e-20:
                        if alig.title.find(sys.argv[1])!=-1:
                            title = cut(alig.title)
                            print "{0}\t{1}\t{2}\t{3}".format(strQuery, title, hsp.score, hsp.expect)

def cut(pStr):
	index=pStr.find(sys.argv[1])
	first_index=pStr[0:index].rfind('|')+1
	last_index=pStr[index:].find(' ')+index
	return pStr[first_index:last_index]
def getQueryString(pXML):
    import xml.etree.ElementTree as ET
    import string
    try:
        root=ET.fromstring(pXML)
    except:
        return ''
    else:
        return root.find('BlastOutput_query-def').text

main()
