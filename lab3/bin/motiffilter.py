#! /usr/bin/env python2
# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics
# Course code:	DD2404
# Date:		2012-12-01  

import sys
import re
from Bio import SeqIO

def main():

    if len(sys.argv)==1:
        sys.stderr.write("ERROR: missing filename\n")
        sys.stderr.write("Usage: ./motiffilter.py FILE\n")
        sys.exit()
    try:
        f = open(sys.argv[1])
        if f.read(1)!=">":
            sys.stderr.write("ERROR: {0} is probably not a FASTA file!\n".format(sys.argv[1]))
            sys.stderr.write("Input file must contain protein sequences in FASTA format\n".format(sys.argv[1]))
            sys.exit()
        f.seek(0)
    except IOError as fErr:
        sys.stderr.write("ERROR: cannot open {0}!\n".format(sys.argv[1]))
        sys.exit()

    for s in SeqIO.parse(f,"fasta"):
        if motiffilter(str(s.seq)):
            print s.format("fasta")

    return 0

def motiffilter(pStr):
    '''
    \param pStr: is a sequence which must be in capital characteres.
    Return true when pStr contain a given motif: K-L-[EI]{2-}-K (in Prosite notation).
    i.e., we want to extract those sequences that contain KL followed by two or more of either E or I, then a K.
    '''
    pattern=re.compile(".*KL[EI][EI]+K.*")
    if pattern.match(pStr):
        return True
    else:
        return False

main()
