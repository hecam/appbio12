#! /usr/bin/env python
# Authors:
#       	Marco Antonio Marra
#       	Jose Maria Facil
# Course:   	Applied Bioinformatics
# Course code:  DD2404
# Date:     	2012-12-01

import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.Data import CodonTable

def main():
    
    if len(sys.argv) == 1:
        sys.stderr.write("ERROR: missing filename\n")
        sys.stderr.write("Usage: ./translatedna.py FILE\n")
        sys.exit()
    try:
        f = open(sys.argv[1])
        if f.read(1) != ">":
            sys.stderr.write("ERROR: {0} is probably not a FASTA file!\n".format(sys.argv[1]))
            sys.stderr.write("Input file must contain DNA sequences in FASTA format\n".format(sys.argv[1]))
            sys.exit()
        f.seek(0)
    except IOError as fErr:
        sys.stderr.write("ERROR: cannot open {0}!\n".format(sys.argv[1]))
        sys.exit()
    
    for s in SeqIO.parse(f,"fasta"):
        try:
            s.seq=s.seq.translate()
            print s.format("fasta").rstrip()
        except CodonTable.TranslationError as tErr:
            sys.stderr.write("ERROR: the '{0}' entry is not a DNA sequence!\n".format(s.id))
            sys.stderr.write("{0}\n".format(tErr.message))

    return 0

main()
