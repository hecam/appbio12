#! /usr/bin/env python2
# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics
# Course code:	DD2404
# Date:		2012-  -  

from Bio import SeqIO
import sys
import pexpect
import tempfile

try:
    mFile = sys.argv[1]
except IndexError:
    sys.stderr.write("Error: You have to specify the name of the file.\n")
    sys.stderr.write("\nUsage:\n    bootstrap <filename> <number of boostraps>\n")
    sys.exit()
try:
    mNum = sys.argv[2]
    mNum = int(mNum)
    mNum = str(mNum)
except IndexError:
    sys.stderr.write("Error: You have to specify the number of bootstraps.\n")
    sys.stderr.write("\nUsage:\n    bootstrap <filename> <number of boostraps>\n")
    sys.exit()
except ValueError:
    sys.stderr.write("Error: The number of bootstraps is not a number\n")
    sys.stderr.write("\nUsage:\n    bootstrap <filename> <number of boostraps>\n")
    sys.exit()


try:
    records = list(SeqIO.parse(mFile,"fasta"))
except IOError as e:
    sys.stderr.write("Error: {0}\n".format(e.strerror))
    sys.exit()

longNames = {}
for i, record in enumerate(records):
    longNames[i] = record.id
    record.id = "seq%i" % i

tmpFile = tempfile.NamedTemporaryFile()
try:
    SeqIO.write(records,tmpFile,"phylip")
except ValueError:
    sys.stderr.write("Error: The file must be in fasta format\n")
    sys.stderr.write("\nUsage:\n    bootstrap <filename> <number of boostraps>\n")
    sys.exit()
tmpFile.seek(0)

#seqboot section
pexpect.run("rm -f infile outfile")
child = pexpect.spawn("phylip seqboot")
sendList = [tmpFile.name, "R", mNum, "2", "Y", "13"]
for redin in sendList:
    child.sendline(redin)
child = pexpect.spawn("mv outfile infile")
child.close()

#protdist section
child = pexpect.spawn("phylip protdist")
child.sendline("Y")
child = pexpect.spawn("mv outfile infile")
child.close()

#neighbor section
child = pexpect.spawn("phylip neighbor")
child.sendline("Y")
child.expect("Done.")
child.close()
f = open("outtree")
for line in f:
    for i in longNames:
        line = line.replace(records[i].id, longNames[i])
    print line.rstrip(),
f.close()
pexpect.run("rm infile outfile outtree")
