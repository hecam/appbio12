#! /usr/bin/env python
def gccontent(pGenome):
	'''
	gccontent(pGenome):
	/param pGenome: is a sequence of nucleotides which make the genome.
		It must be in CAPITAL characters.
	Return the GCcontent in pGenome.
	'''
	lNumNucleotides=len(pGenome)
	lGCcount=pGenome.count('C')+pGenome.count('G')
	return float(lGCcount)/float(lNumNucleotides)
def readgenome(pFileName):
	'''
	readgenome(pFileName):
	/param pFileName: is the name of a file in fasta format which contains
					a genome DNA sequence.
	Return the sequence.
	'''
	lGenome=''
	lFile=open(pFileName,'r')
	lLine=lFile.readline()
	lLine=lFile.readline()
	while lLine!='' :
		lLine=lLine.rstrip()
		lLine=lLine.upper()
		lGenome=lGenome+lLine
		lLine=lFile.readline()
	lFile.close()
	return lGenome

## MAIN
import sys
if len(sys.argv) == 1:
    sys.stderr.write("Error: no file given!\n")
    sys.stderr.write("\nUsage: ./gccontent <file1> <file2> ...\n")
    sys.exit()
for mFileName in sys.argv[1:]:
	gcc=gccontent(readgenome(mFileName))
        print gcc
