#! /usr/bin/env python
# basedist computes and print the distance matrix among a set of genomic sequences
#
# Input: the filenames of the genomic sequences files in FASTA format

def main():
    Files = sys.argv[1:]
    for fName in sys.argv[1:]:
        try:
            f = open(fName)
            if f.read(1) != ">":
                Files.remove(fName)
                sys.stderr.write("\nWARNING: {0} is probably not a FASTA file and it will be discarded!\nInput files must contain one chromosome sequence in FASTA format only!\n".format(fName))
        except IOError as e:
            Files.remove(fName)
            sys.stderr.write("\nWARNING: file {0} does not exist!\nIt will be discarded!\n".format(fName))
    if len(Files) == 0:
        sys.stderr.write("\nThe program is terminated: no file to process.\n")
        sys.exit()
    PiList = piList(Files)
    matrix = makeMatrix(PiList)
    printphylip(matrix,Files) 
    return 0

def printphylip(pMatrix, pFiles) :
    lNames=list()
    for lFile in pFiles :
        lNames.append(readfasta(lFile,0))
    print '\t'+str(len(lNames))
    for i in range(len(pMatrix)) :
        lName=lNames[i][1:10]
        lName=lName.rstrip().ljust(10)
        lDist=''
        for j in range(len(pMatrix[0])) :
            lDist=lDist+'\t'+str(round(pMatrix[i][j],3))
        print lName+lDist[1:]
            
#given the composition array, compute the distance matrix
def makeMatrix(pPiList):
    """
    makeMatrix(pPiList)
    /param pPiList: list of lists of 4 elements [x1,x2,x3,x4]
    Return the 2D list (matrix) of the distances among the sequences.
    """
    mat = [[ 0.0 for i in range(len(pPiList))] for j in range(len(pPiList))]
    for c1 in range(len(pPiList)):
        for c2 in range(c1, len(pPiList)):
            dist = diff(pPiList[c1], pPiList[c2])
            mat[c1][c2] = dist
            mat[c2][c1] = dist
    return mat

def diff(pPiX,pPiY):
    '''
    diff(pPiX,pPiY):
    /param pPiX: list of floats of 4 elements [x1,x2,x3,x4] where x1+x2+x3+x4=1
    /param pPiY: a list of floats which have the same conditions than pPiX
    Return the measuring difference in composition.
    '''
    if len(pPiX)!=4 or len(pPiY)!=4:
        print >> sys.stderr, 'error!' #ERROR!
    lDiff=0.0
    for i in range(len(pPiX)):
        lDiff=lDiff+math.pow(pPiX[i]-pPiY[i],2)
    lDiff=lDiff*0.25
    lDiff=math.sqrt(lDiff)
    return lDiff

def pi(pSequence):
    '''
    pi(pSequence,):
    /param pSequence: String, sequence of genomes
    Return a list of 4 elements which are the nucleotides frequence
    ,e.g[A,C,G,T]
    '''
    pSequence=pSequence.upper()
    lPiS=list()
    pSequence=pSequence.replace('U','T')
    pSequence=pSequence.replace('R','AG')
    pSequence=pSequence.replace('Y','CTU')
    pSequence=pSequence.replace('K','GTU')
    pSequence=pSequence.replace('M','AC')
    pSequence=pSequence.replace('S','CG')
    pSequence=pSequence.replace('W','ATU')
    pSequence=pSequence.replace('B','CGT')
    pSequence=pSequence.replace('D','AGT')
    pSequence=pSequence.replace('H','ACT')
    pSequence=pSequence.replace('V','ACG')
    pSequence=pSequence.replace('N','ACGT')
    pSequence=pSequence.replace('-','')
    pSequence=pSequence.replace('X','')
    lLen=float(len(pSequence))
    lA=float(pSequence.count('A'))/lLen 
    lC=float(pSequence.count('C'))/lLen
    lG=float(pSequence.count('G'))/lLen 
    lT=float(pSequence.count('T'))/lLen
    lPiS.append(lA)
    lPiS.append(lC)
    lPiS.append(lG)
    lPiS.append(lT)
    return lPiS

def piList(pFiles):
    '''
    '''
    lPiList=list()
    for lFile in pFiles :
        lSequence=readfasta(lFile,1)
        lpi=pi(lSequence)
        lPiList.append(lpi)
    return lPiList

def readfasta(pFile,pOption):
    '''
    if pOption is 0 return the name, if it is 1 return the sequence
    and if it is 2 return both of them in a list    
    '''
    lList=list()
    lFile=open(pFile)
    lList.append(lFile.readline()) # the seq's name has been added into the list
    lSequence=''
    lLine=lFile.readline()
    while lLine!='' and pOption!=0 :
        lLine=lLine.rstrip()
        if lLine[0]=='>':
            sys.stderr.write("\nWARNING: {0} Has more than one sequence!\n".format(lList[0]))
            sys.exit()
        lSequence=lSequence+lLine
        lLine=lFile.readline()
        lList.append(lSequence)
    lFile.close()
    if pOption==2:
        return lList
    else:
        return lList[pOption]

import math
import sys
main()
