\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage{cite}
\usepackage{graphicx}
\usepackage{caption}
\title{Project Report \\ How often are domains single exons?}
\author{DD2404 Applied Bioinformatics}

\begin{document}
\maketitle 

\begin{table}[h]
\captionsetup{format=plain,singlelinecheck=false,justification=justified,font={large,sf,bf}}
\caption*{Group:}
\begin{tabular}{ l l l l }
  Name & Email & Phone & Personal number \\ \hline
  José María Fácil & jmfl@kth.se & 076 716 00 54 & 921215-T097 \\
  Marco Antonio Marra & mamarra@kth.se & 076 710 36 10 & 890613-T290 \\
\end{tabular}
\end{table}

\section*{Objective}
The objective with this assignment is to determine the correlation between exons and domains.

\begin{abstract}
We performed a genome-wide analysis to assess the possible correlation existing between the exon sequences and the protein domain architecture of genes in the human genome. A significant correlation between pairs of single exon borders and pairs of domain borders was observed at the genomic level. Further, we found that in more than two thirds of the times the domains cross the exon borders, within a certain range. These findings perfectly agree with the exon-shuffling theory, by which domains would be predominantely evolving within a single exon, thereby facilitating easier ``domain exchange'' among genes.
\end{abstract}


\section{Introduction}
One working hypotesis in evolution theory is the exon shuffling, proposed by Gilbert \cite{Liu_Grigoriev_2004}. According to that hypotesis the proteins within the genome would easily exchange domains by splitting and re-arranging already existent exon structures, thus increasing the proteome complexity, without changing the total amount of genetic content.

Several studies are found in literature, that deal with these issues, but not so many describe the extent of such a correlation between single exon and domain structures in a genome-wide sense. That is the purpose of this study. We detected an exceedingly large number of exon domain border pairs matchings than expected, according to the hypotesis that exon shuffling was a core process in the increasing of complexity during  evolution.

\section{Methods and Materials}
Species are found to become more adaptive to changes, thanks to a process of recombination of exons, the so called exon shuffling \cite{Liu_Grigoriev_2004}. Thus, a correlation between functional domains and exon sequences would give evidence to this theory. In the present study we choosed to analyze the human genome, and we extract the statistical extent of correlation between each exon pair of borders and every domain borders in the correspondent transcript.
First we downladed the exon data from 17103 genes using Ensembl BioMart website tool: \begin{verbatim}http://www.ensembl.org/biomart/martview\end{verbatim} version 70 for the human genome. Protein domain annotations were downloaded from the Pfam FTP website: \begin{verbatim}ftp://ftp.sanger.ac.uk/pub/databases/Pfam/
    current_release/proteomes/9606.tsv.gz\end{verbatim} where the current version corresponds to 26.0. Since the protein annotation file reported only the UniProt/SwissProt accession, for identifying the proteins, we also needed to create and download a map file, to create a linkage between the UniProt/SwissProt accessions and the Ensembl Protein accessions. Again, it was fairly simple to do through the Ensembl BioMart website tool.

The Pfam algorithms and databases do not take into account exon borders positions to define the domain structures, thus the study will not suffer of any bias, in that sense. An issue came from the fact that genes often present many transcripts, thus diverse proteins might be encoded within the same gene. Consequently we estabilished a criterion to choose only one transcript per gene, and we selected the one with the longest coding sequence, in order to allow the best domain structures coverage. In order to permit some variability and/or errors in the definition of domain borders, we decided to define a ``domain border box'' as a window of amino acids around the domain border positions. Then, for each gene we examined whether the exon borders belonging to single exons are found inside these domain border boxes or not. Also, for final statistical computation matters, the total number of exon borders inside all domain border boxes was extracted.

We only specified few criteria of gene inclusion: for example, we only examined genes with more than one exons and more than one exon borders falling inside the protein sequence. To give evidence to the exon shuffling theory, a higher than expected number of both exon borders should be found to fall inside the respective domain border boxes.

\section{Results and Discussion}
As a consequence of the aforementioned inclusion criteria, a total number of 14894 transcripts and correspondent proteins were examined (Table 1). The presence of exon borders was assessed inside domain border boxes made of 5 amino acids outside and 5 amino acids inside the domain structure. As a result, a higher than expected number of exon borders falling inside these 10 amino acid-boxes was recorded (401 vs 155), thus a significant correlation between single exons and domain structures was shown ($p\textrm{-value}<10^{-86}$), to signify that events of exon recombinations within the entire genome have been very frequent during evolution.

\begin{table}
\captionsetup{format=plain,font=sf}
\caption{Exon borders-domain borders correlation in the Human genome}
\scalebox{0.625}{%
\begin{tabular}{ l l l l l l l l } \hline
  \textbf{Species\textsuperscript{a}} & \textbf{Proteins\textsuperscript{b}} & \textbf{Domains\textsuperscript{c}} & \textbf{Exon borders\textsuperscript{d}} & \textbf{Expected ($E$)\textsuperscript{e}} & \textbf{Observed ($O$)\textsuperscript{e}} & \textbf{$(O-E)/E\%$\textsuperscript{e}} & \textbf{$p\textrm{-value}$\textsuperscript{e}} \\ \hline
  Human & 14894 & 36316 & 154460 & 155 & 401 & 159 & 5.49E-87 \\ \hline
\end{tabular}
}
\\
\captionsetup{format=plain,justification=justified,font={scriptsize,sf}}
\caption*{\textbf{\textsuperscript{a}}Statistics were obtained for the Human eukaryotic genome provided by Ensembl. Only one transcript (the longest coding sequence) per gene was used. Single-exon genes and genes with no more than one exons inside coding sequence were excluded. An E-value cutoff of 0.05 was required for the domain definitions provided by Pfam.\\
\textbf{\textsuperscript{b}}The total number of protein processed.\\
\textbf{\textsuperscript{c}}The total number of domains found within all proteins.\\
\textbf{\textsuperscript{d}}The total number of exon borders found within all proteins.\\
\textbf{\textsuperscript{e}}We calculated the expected number of exon borders falling inside domain border boxes, as if they were randomly distribuited along the whole protein sequences. We modelled the problem using the hypergeometric distribution, to simulate the extraction of the total number of exon borders, without replacement (see appendix). The null hypotesis of exon borders being randomly distributed was then tested against the observation, and the chi-square $p\textrm{-value}$ of the test was obtained, given $E$, $O$ and the total number of all exon borders.}
\end{table}

\newpage
\appendix
\section{Appendix}
The hypergeometric distribution was used to describe the problem of sampling without replacement, which occurs when we search for two consecutive events of exon borders (belonging to the same exon) falling inside the domain border boxes of one domain. The expected value of this distribution is given by

\begin{center}
\begin{math}
E = \dfrac{rh}{n}
\end{math}
\end{center}
where $r$ is the number of random samplings (the total number of exon borders), $h$ is the number of successes expected (the expected number of exon borders falling inside domain border boxes), $n$ is the size of the population (the total length of the proteins surveyed, in amino acids).

Being $n$ and $r$ already defined, $h$ has yet to be found. In the hypotesis of randomly distributed exon borders along the whole genome, one can express the probability to find an exon border over any amino acid of the whole sequence as

\begin{center}
\begin{math}
P = \dfrac{\textrm{total number of exon borders}}{\textrm{total length of proteins surveyed}}
\end{math}
\end{center}
thus the expected number of exon borders falling inside domain border boxes is simply given multiplying P by the total length of domain border boxes, in amino acids, $T$:

\begin{center}
\begin{math}
h = P \cdot T
\end{math}
\end{center}
Finally, we can express $E$ as

\begin{center}
\begin{math}
\begin{array}{l}
E = \\ \\
= \dfrac{r}{n} \cdot \dfrac{\textrm{total number of exon borders}}{\textrm{total length of proteins surveyed}} \cdot T = \\ \\
= \dfrac{\textrm{total number of exon borders}^{2}}{\textrm{total length of proteins surveyed}^{2}} \cdot T
\end{array}
\end{math}
\end{center}

\newpage
\bibliographystyle{ieeetr}
\bibliography{ref}

\end{document}