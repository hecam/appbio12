#!/usr/bin/env python
# Authors:
#           Marco Antonio Marra
#           Jose Maria Facil
# Course:       Applied Bioinformatics
# Course code:  DD2404
# Date:         2013-01-15
#
# Core script

import sys
import csv
import numpy
from scipy.stats import chisquare
from pardef import *

def main():
    '''
    Main function that performs the driving tasks, call the functions to create
    the data structures, and the main job.
'''

    sys.stdout.write('Loading the protein mapping...')
    sys.stdout.flush()
    try:
        fMap=open(fileMapName)
    except IOError:
        sys.stderr.write('\nERROR! Cannot open {0}!\n'.format(fileMapName))
        sys.exit()
    #load the protein mapping
    Map=createMap(fMap)
    fMap.close()
    sys.stdout.write('Done\n')
    
    sys.stdout.write('Loading the proteome dataset...')
    sys.stdout.flush()
    try:
        fProt=open(fileProteomeName)
    except IOError:
        sys.stderr.write('\nERROR! Cannot open {0}!\n'.format(fileProteomeName))
        sys.exit()
    #load the proteome data
    Prot=createProteome(fProt, E)
    fProt.close()
    sys.stdout.write('Done\n')
    
    sys.stdout.write('Carrying the genome-wide task...')
    sys.stdout.flush()
    try:
        fGen=open(fileGenomeName)
    except IOError:
        sys.stderr.write('\nERROR! Cannot open {0}\n'.format(
            fileGenomeName))
        sys.exit()
    #load the exons data and compute the task
    [NObs, NProt, NExBord, NDom, NCross, LProt, LBoxTot, NExObs
            ]=compGenome(fGen, NGenes, Prot, Map, w)
    fGen.close()
    sys.stdout.write('Done\n')
    
    #do statistical computations
    NExp=float(NExBord**2)/(LProt**2)*LBoxTot
    perc=int(round(100*(float(NObs)/float(NExp)-1)))
    NExp=int(round(NExp))
    pval='%.3g' % chisquare(numpy.int_([NObs, NExBord-NObs]), numpy.int_(
            [NExp, NExBord-NExp]))[1]
    
    #organize the results in a dictionary and write to file
    with open(fileResultsName, 'w') as fOut:
        fOut.write(str({'w':w, 'E-value':E, 'NGenes':NGenes, 'NObs':NObs,
                'NProt':NProt, 'NExp':NExp, 'NExBord':NExBord, 'NDom':NDom,
                'NCross':NCross, 'LProt':LProt, 'LBoxTot':LBoxTot,
                'NExObs':NExObs, 'perc':perc, 'p-value': pval}))
    
    return 0


def compGenome(fGen, NGenes, Prot, Map, w):
    '''
Core function that organizes the workflow through the genome. It cycles along
the whole genome transcripts, and it increments the many counters.
Input:
    fGen: file object for the genome data file
    NGenes: int, number of genes in the study, if present
    Prot: the proteome dictionary
    Map: the Ensembl/UniProt codes mapping dictionary
    w: tuple of int for the domain border box definition, ex. (5,1)
Output:
    NObs: int, total exons-domains matchings
    NProt: int, total number of proteins surveyed
    NExBord: int, total number of exon borders inside all proteins
    NDom: int, total number of domains analyzed
    NCross: int, number of exon border boundary crossings
    LProt: int, total length of the proteins surveyed, in aa
    LBoxTot: int, total number of amino acids inside domain border boxes
    NExObs: int, total number of exon borders inside domain border boxes
'''
    
    #prepare a dictionary for the genome data
    Gen=dict()
    #create a tsv reader object
    fGenReader=csv.reader(fGen, dialect="excel-tab")
    #store the field names
    try:
        fieldnames=fGenReader.next()
    except StopIteration:
        sys.stderr.write('\nERROR! The genome file may be corrupted!\n')
        sys.exit()
    #read the first data row in a list
    rows=[fGenReader.next()]
    EOF=False   #flag to signal the end of the file reached
    NProt=0     #total number of proteins
    NDom=0      #total number of domains found
    NObs=0      #total number of exon-domain matchings
    NCross=0    #total number of exon border crossings
    NExBord=0   #total number of exon borders inside all proteins
    LProt=0     #total length of all proteins, in aa
    LBoxTot=0   #total number of amino acid inside domain border boxes, in aa
    NExObs=0    #total number of single exon borders inside domain border boxes
    i=0
    sys.stdout.write('\n')
    #loop until the end of file is reached
    while not EOF:
        try:
            #read the next data row and save in a variable
            nextRow=fGenReader.next()
            #aggregate rows (transcripts) of the same gene
            if nextRow[0]==rows[0][0]:
                rows.append(nextRow)
            #process the current gene data
            else:
                #having the total number of genes
                try:
                    i+=1
                    sys.stdout.write('\r'+str(int(float(i)/NGenes*100))+'%\t')
                #missing the total number of genes
                except:
                    sys.stdout.write('\r'+str(int(float(i)))+' genes processed\t')
                #print the number of hits found `on-line'
                finally:
                    sys.stdout.write(str(NObs)+' hits found'+'\r')
                #rearrange the data structure to have tuples for each field
                columns=zip(*rows)
                #store the next gene row for the next loop
                rows=[nextRow]
                #create a dictionary with field name keys and tuples as values
                for field in enumerate(fieldnames):
                    Gen[field[1]]=columns[field[0]]
                #trim the non-coding exons
                Gen=trimGen(Gen)
                #choose which transcript
                transcriptID=whichTranscriptID(Gen)
                #trim other transcripts
                Gen=getTranscript(transcriptID, Gen)
                #exclude one-exon genes
                if len(Gen['Ensembl Transcript ID'])==1:
                    continue
                #call function to find the UniProt accession
                protAcc=findProtAcc(transcriptID, Gen, Map)
                #call function to search the protein
                if not findProt(protAcc, Prot):
                    continue
                NProt+=1
                #obtain the protein data set for the current transcript
                [prot, nDom, lProt, pb]=compProt(protAcc, Gen, Prot)
                #compute the domain count for the current gene
                [a, b, c, d]=domCount(Gen, prot, w, pb)
                NObs+=a         #increment the total number of observation
                NCross+=b       #increment the total number of crossings
                NDom+=nDom      #increment the total number of domains found
                #count the exon borders in this transcript
                NExBord+=len(Gen['Ensembl Transcript ID'])-1
                LProt+=lProt    #increment the total length of proteins
                LBoxTot+=c      #increment the total number of aa inside boxes
                NExObs+=d       #increment the number of exon borders inside
        #end of file reached
        except StopIteration:
            EOF=True
            sys.stdout.write('\r'+' '*80+'\r')
        
    return [NObs, NProt, NExBord, NDom, NCross, LProt, LBoxTot, NExObs]


def compProt(protAcc, pGen, pProt):   
    '''
Create a small dataset of the proteome, given the protein accession. Calculate
the length of the current protein (in aa) and the protein borders positions.
Input:
    protAcc: string for the protein UniProt accession
    Gen: the genome dictionary
    Prot: the full proteome dataset dictionary
Output:
    prot: smaller proteome dictionary, each key is a field string and the
          values are lists of strings
    nDom: int, number of domains in the current protein definition
    lProt: int, length of the current protein, in aa
    pb: tuple of int, positions of the current protein borders, in bp
'''
    #store the position of the left protein borders, in bp
    p1=int(pGen['cDNA coding start'][0])
    #store the position of the right protein borders, in bp
    p2=int(pGen['cDNA coding end'][-1])
    #put the protein border positions in a tuple
    pb=(p1, p2)

    #measure the length of the current protein, in aa
    lProt=(p2-p1+1)/3

    #initialize a new small proteine set
    prot=dict()
    for field in pProt.keys():
        prot[field]=list()
    j=-1
    #fill in a small protein set corresponding to the current protein accession
    while True:
        try:
            #store the index of the next protein accession
            j=pProt['<seq id>'].index(protAcc,j+1)
        except:
            #jump out if last item was reached
            break
        #fill in the dictionary with the current protein domains
        for field in pProt.keys():
            prot[field].append(pProt[field][j])

    #count the number of domains in this protein
    nDom=len(prot['<seq id>'])
    
    return [prot, nDom, lProt, pb]


def domCount(pGen, prot, w, pb):
    '''
Perform a series of counts, the number of exon borders inside domain border
boxes, the number of exons-domains matchings, the number of exon border
boundary crossings and the number of amino acids inside domain border boxes.
Input:
    Gen: the genome dictionary
    prot: the small set of the proteome dictionary
    w: tuple of int for the domain border box definition, in aa, ex. (5, 1)
    pb: tuple of int for the protein borders positions, in bp, ex. (130, 444)
Output:
    nObs: int, number of exons-domains matchings
    nCross: int, number of exon border boundary crossings
    lBox: int, number of amino acidsinside domain border boxes
    nExObs: int, number of exon borders inside domain border boxes
'''

    #store the left protein border position, in bp
    p1=pb[0]
    #store the right protein border position, in bp
    p2=pb[1]
    nObs=0      #count the domain/exon matchings in this protein
    nCross=0    #count the exon crossings in this protein
    lBox=0      #count the total number of aa in domain border boxes
    nExObs=0    #count the total exon/domain border matchings in this protein
    #loop over the UniProt accessions
    for i in range(len(prot['<seq id>'])):
        #store the position of the left domain border, in aa
        d1=int(prot['<envelope start>'][i])
        #store the position of the right domain border, in aa
        d2=int(prot['<envelope end>'][i])
        #ignore `unboxable' border domains
        if not((3*(d1-w[0])>=p1 and 3*(d2+w[0])<=p2) and 
                (3*(d1+w[1]-1)<p2 and 3*(d2-w[1]+1)>p1)):
            continue
        #count the right side amino acids inside the domain border box, in bp
        if p2>=3*(d2+w[0]):
            lBox+=3*(w[0]+w[1])
        else:
            lBox+=p2-3*(d2+w[1])
        #count the left side amino acids inside the domain border box, in bp
        if p1<=3*(d1-w[0]):
            lBox+=3*(w[0]+w[1])
        else:
            lBox+=3*(d1+w[1])-p1
        #loop over any exons of this transcript
        for k in range(len(pGen['Ensembl Transcript ID'])):
            #store the positions of the left exon border, in bp
            x1=int(pGen['cDNA coding start'][k])
            #store the positions of the right exon border, in bp
            x2=int(pGen['cDNA coding end'][k])
            #left exon border falling into the left domain border box
            if x1<=3*(d1+(w[1]-1)) and x1>3*(d1-(w[0]+1)):
                #score in the total number of exon borders
                nExObs+=1
                #right exon border falling into the right domain border box
                if x2<=3*(d2+w[0]) and x2>3*(d2-w[1]):
                    #score one observation if both criteria are fulfilled
                    nObs+=1
                    #any of the domain borders crosses the exon borders
                    if x1>3*(d1-1)+1 or 3*d2>x2:
                        #score one crossing
                        nCross+=1
            #right exon border only falling into the right domain border box
            if x2<=3*(d2+w[0]) and x2>3*(d2-w[1]):
                #score in the total number of exon borders
                nExObs+=1
    #convert the number of aa inside domain border boxes from bp to aa
    lBox/=3
    
    return [nObs, nCross, lBox, nExObs]


def trimGen(pGen):
    '''
Given the current genome dictionary, remove the non coding exons.
Input:
    Gen: the untrimmed genome dictionary
Output:
    Gen: the trimmed genome dictionary
'''

    for field in pGen.keys():
        pGen[field]=list(pGen[field])
    #remove non coding exons from the transcript
    i=0
    while i<len(pGen['Ensembl Gene ID']):
        if (list(pGen['cDNA coding start'])[i]==''):
            for field in pGen.keys():
                pGen[field].pop(i)
        else:
            i+=1

    return pGen


def findProt(protAcc, pProt):
    '''
Checks whether the given protein accession is present or not in the proteome
dictionary.
Input:
    protAcc: string for the protein UniProt accession
    Prot: the proteome dictionary
Output:
    True/False (boolean logic variable)
'''

    #search the accession in the proteome
    try:
        i=pProt['<seq id>'].index(protAcc)
    except:
        return False

    return True


def createMap(fMap):
    '''
Loads the protein codes mapping from file.
Input:
    fMap: file object for the mapping data file
Output:
    Map: the Ensembl/UniProt codes mapping dictionary.
         The keys are the strings for the field names of the TSV file, the
         values are tuples of strings for the protein codes.
'''

    Map=dict()
    #with fMap:
    fMapReader=csv.reader(fMap, dialect="excel-tab")
    try:
        fieldnames=fMapReader.next()
    except StopIteration:
        sys.stderr.write('\nERROR! The map file may be corrupted!\n')
        sys.exit()
    columns=zip(*fMapReader)
    for field in enumerate(fieldnames):
        Map[field[1]]=columns[field[0]]

    return Map


def createProteome(fProt, pE):
    '''
Loads the proteome definition from file.
All the domain entries with an E-value below E are ignored.
Input:
    fProt: file object for the proteome data file
    E: floating point E-value cutoff number
Output:
    Prot: the proteome dictionary. Each key corresponds to one field in the TSV
          file, and the values are lists of strings
'''

    Prot=dict()
    for i in range(2):
        fProt.readline()
    fieldnames=fProt.readline().rstrip().replace('> <','>\t<').lstrip(
            '#').split('\t')
    fProtReader=csv.reader(fProt, dialect="excel-tab")
    columns=zip(*fProtReader)
    try:
        for field in enumerate(fieldnames):
            Prot[field[1]]=list(columns[field[0]])
    except:
        sys.stderr.write('\nERROR! The proteome file may be corrupted!\n')
        sys.exit()

    i=0
    while True:
        try:
            if not float(Prot['<E-value>'][i])<pE:
                for field in Prot.keys():
                    Prot[field].pop(i)
            else:
                i+=1
        except:
            break

    return Prot


def whichTranscriptID(pGen):
    '''
Choose which of the available transcripts to consider. The criterion is based
on the longest (bp length) coding region found among the many.
Input:
    Gen: the genome dictionary
Output:
    transcriptID: string for the Ensembl Transcript ID
'''
    
    #take the first found transcript ID
    #transcriptID=pGen['Ensembl Transcript ID'][0]
    
    i=0
    Lmax=0
    #take the longest transcript found
    while i<len(pGen['Ensembl Transcript ID']):
        transcript=pGen['Ensembl Transcript ID'][i]
        N=pGen['Ensembl Transcript ID'].count(transcript)
        L=int(pGen['cDNA coding end'][i+N-1])-int(pGen['cDNA coding start'][i])
        if L>Lmax:
            Lmax=L
            transcriptID=pGen['Ensembl Transcript ID'][i]
        i+=N

    return transcriptID


def getTranscript(transcriptID, pGen):
    '''
Given the Ensembl Transcript ID, returns a copy of the genome dictionary
containing only the correspondent entries.
Input:
    transcriptID: string for the Ensembl Transcript ID
    Gen: the genome dictionary
Output:
    Gen: the genome dictionary limited to the current transcript
'''

    #store the index of the first exon in the current transcripts tuple
    exInd=pGen['Ensembl Transcript ID'].index(transcriptID)
    
    #store the index of the last exon in the current transcript tuple
    exLast=exInd+pGen['Ensembl Transcript ID'].count(transcriptID)

    #remove the other transcripts
    for field in pGen.keys():
        pGen[field]=pGen[field][exInd:exLast]

    return pGen

 
def findProtAcc(transcriptID, pGen, pMap):
    '''
Given the Ensembl Transcript ID, searches the correspondent UniProt Accession
through the proteins map.
Input:
    transcriptID: string for the Ensembl Transcript ID
    Gen: the genome dictionary
    Map: the Ensembl/UniProt codes mapping dictionary
Output:
    protAcc: string for the protein UniProt accession
'''

    #find the Ensembl protein ID
    protID=pGen['Ensembl Protein ID'][pGen['Ensembl Transcript ID']
            .index(transcriptID)]
    
    #find the correspondent UniProt Accession
    protAcc=pMap['UniProt/SwissProt Accession'][
            pMap['Ensembl Protein ID'].index(protID)]
    
    return protAcc


main()
