#!/usr/bin/env python
# Authors:
#           Marco Antonio Marra
#           Jose Maria Facil
# Course:       Applied Bioinformatics
# Course code:  DD2404
# Date:         2013-01-20
#
# Parameters definition

#filename containing the genome transcripts dataset
fileGenomeName='../data/curr_set/exons.txt'

#filename containing the proteine codes mapping
fileMapName='../data/curr_set/protIDs.txt'

#filename containing the proteome definition
fileProteomeName='../data/curr_set/9606.tsv'

#number of genes surveyed
NGenes=17103

#filename of the output results
fileResultsName='../results/curr_res/output'

#domain border box size
w=(5,5)

#E-value cutoff for the domain definitions
E=0.05