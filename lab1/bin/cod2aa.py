#!/usr/bin/env python
# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics 
# Course code:	DD2404
# Date:         2012-11-11
#
# Last update: 2012-11-11
# by: Marco Antonio Marra

def cod2aa(s):
    '''
    This function translate a codon into the correspondant
    amino-acid, according to the Standard Genetic Code.

    Input: a string
    Output: a string of one character long.
    '''
    if len(s) != 3:
        return 'X'
    cod = Ufy(s.upper())
    startStr = '<'
    stopStr = '*'
    makeCode(startStr, stopStr)
    return translate(cod)

def Ufy(raw_cod):
    '''
    This function replaces all the 'U's in the codon with 'T's.
    '''
    cod = ''
    for c in raw_cod:
        if c == 'U':
            cod += 'T'
        else:
            cod += c
    return cod

def translate(cod):
    for aa in stdGenCode:
        if cod in stdGenCode[aa]:
            break
    return aa

def makeCode(startStr, stopStr):
    '''
    This function generates the Standard Genetic Code in the form
    of a dictionary, where each key corresponds to an amino-acid,
    to which a tuple of codons is associated.
    Input: the strings which identify the START and STOP codons.
    '''
    global stdGenCode
    stdGenCode = {
            'A' : ('GCT', 'GCC', 'GCA', 'GCG', 'GCN'),
            'R' : ('CGT', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG', 'CGN', 'MGR'),
            'N' : ('AAT', 'AAC', 'AAY'),
            'D' : ('GAT', 'GAC', 'GAY'),
            'C' : ('TGT', 'TGC', 'TGY'),
            'Q' : ('CAA', 'CAG', 'GAR'),
            'E' : ('GAA', 'GAG', 'GAR'),
            'G' : ('GGT', 'GGC', 'GGA', 'GGG', 'GGN'),
            'H' : ('CAT', 'CAC', 'CAY'),
            'I' : ('ATT', 'ATC', 'ATA', 'ATH'),
            'L' : ('TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG', 'YTR', 'CTN'),
            'K' : ('AAA', 'AAG', 'AAR'),
            'M' : ('ATG'),
            'F' : ('TTT', 'TTC', 'TTY'),
            'P' : ('CCT', 'CCC', 'CCA', 'CCG', 'CCN'),
            'S' : ('TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC', 'TCN', 'AGY'),
            'T' : ('ACT', 'ACC', 'ACA', 'ACG', 'ACN'),
            'W' : ('TGG'),
            'Y' : ('TAT', 'TAC', 'TAY'),
            'V' : ('GTT', 'GTC', 'GTA', 'GTG', 'GTN'),
            startStr : ('ATG'),
            stopStr : ('TAA', 'TGA', 'TAG', 'TAR', 'TRA')
            }
    return 0
