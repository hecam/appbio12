#! /usr/bin/env python


# Authors:
#       Marco Antonio Marra
#       Jose Maria Facil
# Course:   Applied Bioinformatics 
# Course code:  DD2404
# Date:     2012-11-13

import sys
import re

def print_fasta(s):
    '''
    print_fasta:
    /param s is the line of a sequence in a Stockholm format file
        example: prot17 MAGQDPRLRGEPL
    Print the sequence in fasta format with rows at most 
    60 characteres wide
    '''
    pattern=re.compile("[' '\t]+")
    lList=re.split(pattern,s)
    print ">", lList[0]
    lI=1
    while lI<len(lList) and len(lList[lI])==0:
        lI+=1
    if lI>=len(lList):
        print ''
    else:
        lSeq=lList[lI]
        lSeq=lSeq.upper()
        lIndex=0
        while len(lSeq[lIndex:])>60:
            print lSeq[lIndex:lIndex+60]
            lIndex+=60
        if len(lSeq[lIndex:])>0:
            print lSeq[lIndex:]
def translate_dna(filename):
    '''
    translate_dna :
    /param filename : It's the name of a file which contains a series of DNA sequence.
    '''
    nameseq=''
    seq=''
    f=open(filename)
    line=f.readline()
    while line!='': 
        line=line.rstrip()
        if line[0]=='>' :
            print_seq_aa(nameseq,seq)
            seq=''
            nameseq=line
        else :
            seq=seq+line
        line=f.readline()
    print_seq_aa(nameseq,seq)
    f.close()


def print_seq_aa(name,seq):
    '''
    print_seq_aa:
    /param name: It's the name of the sequence. The format of this string must be:
            name='>name'
    /param seq: seq is the sequence of dna 
    Print the name of the sequence and the longest amino acid sequence is fasta format.
    '''
    seq_aa=''
    if name!='' :
        for i in range(3) :
            seq_aa_aux=aaseq(seq[i:])
            if len(seq_aa)<len(seq_aa_aux) :
                seq_aa=seq_aa_aux
        seq_complet=name[1:]+"\t"+seq_aa
        print_fasta(seq_complet)


def aaseq(s):
    ''' 
    aaseq:
    /param s: s is a string which contains a dna sequence
    Return the amino acid sequence which is equivalent with the dna sequence recived in s.
    '''
    aaseq=''
    i=0
    while len(s[i:(i+3)])==3 :
        aa=cod2aa(s[i:(i+3)])
        if aa!=STOP :
            aaseq=aaseq+aa
            i=i+3
        else :
            break
    return aaseq

def cod2aa(s):
    '''
    This function translate a codon into the correspondant
    amino-acid, according to the Standard Genetic Code.

    Input: a string
    Output: a string of one character long.
    '''
    if len(s) != 3:
        return 'X'
    cod = Ufy(s.upper())
    return translate(cod)

def Ufy(raw_cod):
    '''
    This function replaces all the 'U's in the codon with 'T's.
    '''
    cod = ''
    for c in raw_cod:
        if c == 'U':
            cod += 'T'
        else:
            cod += c
    return cod

# What happens if the cod is not in stdGenCode? it must return 'X' 
def translate(cod):
    done=False
    for aa in stdGenCode:
        if cod in stdGenCode[aa]:
            done=True
            break
    if done:
        return aa
    else:
        return 'X'

def makeCode(startStr, stopStr):
    '''
    This function generates the Standard Genetic Code in the form
    of a dictionary, where each key corresponds to an amino-acid,
    to which a tuple of codons is associated.
    Input: the strings which identify the START and STOP codons.
    '''
    global stdGenCode
    stdGenCode = {
        'A' : ('GCT', 'GCC', 'GCA', 'GCG', 'GCN'),
        'R' : ('CGT', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG', 'CGN', 'MGR'),
        'N' : ('AAT', 'AAC', 'AAY'),
        'D' : ('GAT', 'GAC', 'GAY'),
        'C' : ('TGT', 'TGC', 'TGY'),
        'Q' : ('CAA', 'CAG', 'GAR'),
        'E' : ('GAA', 'GAG', 'GAR'),
        'G' : ('GGT', 'GGC', 'GGA', 'GGG', 'GGN'),
        'H' : ('CAT', 'CAC', 'CAY'),
        'I' : ('ATT', 'ATC', 'ATA', 'ATH'),
        'L' : ('TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG', 'YTR', 'CTN'),
        'K' : ('AAA', 'AAG', 'AAR'),
        'M' : ('ATG'),'F' : ('TTT', 'TTC', 'TTY'),
        'P' : ('CCT', 'CCC', 'CCA', 'CCG', 'CCN'),
        'S' : ('TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC', 'TCN', 'AGY'),
        'T' : ('ACT', 'ACC', 'ACA', 'ACG', 'ACN'),
        'W' : ('TGG'),'Y' : ('TAT', 'TAC', 'TAY'),
        'V' : ('GTT', 'GTC', 'GTA', 'GTG', 'GTN'),
        stopStr : ('TAA', 'TGA', 'TAG', 'TAR', 'TRA')
        }
    return 0


#   MAIN
global STOP
STOP='#'
global START
START='*'
if len(sys.argv)==1 :
    filename=raw_input("Which sequence file?")
else :
    filename=sys.argv[1]

makeCode(START,STOP)
translate_dna(filename)
