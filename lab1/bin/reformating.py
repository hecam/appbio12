#! /usr/bin/env python

# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics 
# Course code:	DD2404
# Date:		2012-10-30

# Python program that reads sequences from Stockholm files and
# re-format the sequence into Fasta format with the sequences broken
# down to rows at most 60 characters wide.

import sys
import re
import string

def print_fasta(s):
	'''
 	print_fasta:
 	/param s is the line of a sequence in a Stockholm format file
 		example: prot17 MAGQDPRLRGEPL
 	Print the sequence in fasta format with rows at most 
 	60 characteres wide
	'''
	pattern=re.compile("[' '\t]+")
	lList=re.split(pattern,s)
	print ">", lList[0]
	lI=1
	while lI<len(lList) and len(lList[lI])==0:
		lI+=1
	if lI>=len(lList):
		print ''
	else:
		lSeq=lList[lI]
		lSeq=lSeq.upper()
		lIndex=0
		while len(lSeq[lIndex:])>60:
			print lSeq[lIndex:lIndex+60]
			lIndex+=60
		if len(lSeq[lIndex:])>0:
			print lSeq[lIndex:]

def isSequence(ln):
	'''
 	isSequence:
 	/param ln is the a line of a Stockholm format file, it can be any line
 		of a file with this format
 	Return true if only if ln is the line of a sequence.
	'''
    isSequence=False
    exp='^(#|//)'
    pattern=re.compile(exp)
    isSequence=not pattern.match(ln)
    return isSequence

def print_ShlmFile(filename):
	''' 
	print_ShlmFile:
	/param filename is the name of a file with Stockholm format.
	Print only those lines in the SF.File which are sequences.
	'''
    with open(filename,'r') as f:
        for ln in f:
            if isSequence(ln):
                ln=ln.rstrip()
		if len(ln)>0:
                	print_fasta(ln)

#main
if len(sys.argv)==1 :
	filename=raw_input("Which squence file?")
else :
	filename=sys.argv[1]
print_ShlmFile(filename)
