#!/usr/bin/env python

# This program receives an integer number as the length of a DNA
# sequence and creates a random DNA sequence with that length.

# Authors:
#		Marco Antonio Marra
#		Jose Maria Facil
# Course: 	Applied Bioinformatics 
# Course code:	DD2404
# Date:		2012-10-29

import random
# possible naucleic acid components
code=['A','C','G','T','U','R','Y','K','M','S','W','B','D','H','V','N','X','-']
# Prompt the user for the length
length=input('Length: ')
myrandomdna=''
for i in range(length):
    # Append random element from code to myrandomdna
    myrandomdna+=random.choice(code[:4])
print 'My random DNA sequence: '
print myrandomdna
